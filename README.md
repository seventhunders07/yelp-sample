## Yelp Sample App

- Search by business name
- Search by address/city/postal code
- Search by cuisine type
- Sorting of results by distance and ratings
- Easy to use UI
- Name and photo of business
- Categories
- Hours of operation
- Address and contact information
- Ratings and snippet


### Extra Work
- used MVVM architecture with observable pattern using LiveData
    - I separated block of work using the MVVM pattern and LiveData to pass the data to the view without the view knowing about the viewmodel, also I separated all API calls to a different class called RemoteRepository.class for easy access and maintenance of the app.
    
    - extend base classes coming from my library to remove any boilerplate codes into the main application.

- Added instrumentation testing using espresso for UI automation testing (see androidTest folder)

- Used single instance retrofit client class for all api calls and put the base url inside the manifest via the metadata however this method removes the testability of the api calls because I can't mock the application from the androidViewModel class because this retrofit client relies on the application context of the app to get the meta-data from the manifest.

- Used [ButterKnife](https://github.com/JakeWharton/butterknife) library for view-binding since I didn't use kotlin, so that I will not have any findViewById calls on the root view.