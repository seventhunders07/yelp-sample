package ph.seventhunders.applibrary.adapter;


import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import ph.seventhunders.applibrary.custom.BaseFragment;

/**
 * Created by root on 3/5/18.
 */

public class DynamicFragmentPagerAdapter extends FragmentPagerAdapter {

    ArrayList<BaseFragment> mPagerScreens = new ArrayList<>();
    ArrayList<String> mPageTitle = new ArrayList<>();
    public DynamicFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mPagerScreens.get(position);
    }

    @Override
    public int getCount() {
        return mPagerScreens.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitle.get(position);
    }

    /**
     * adds screen to view pager
     * @param baseFragment screen to add to view pager
     * @param title title for the screen (e.g tab pager)
     */
    public void addScreen(BaseFragment baseFragment, @Nullable String title){
        mPagerScreens.add(baseFragment);
        mPageTitle.add(title);
    }
}
