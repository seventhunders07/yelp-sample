package ph.seventhunders.applibrary.custom;

import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;


/**
 * Created by root on 2/28/18.
 */

public abstract class BaseDashboardActivity extends BaseActivity {

    NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mActionBarToggle;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initialize();
    }

    private void initialize() {
        mNavigationView = setNavigationView();
        mDrawerLayout = setDrawerLayout();
        mActionBarToggle = setActionBarToggle();
        if (mNavigationView == null) {
            throw new NullPointerException("Navigation view must not be null");
        }
        if (mDrawerLayout == null) {
            throw new NullPointerException("DrawerLayout must not be null");
        }
        if (mActionBarToggle == null)
            throw new NullPointerException("ActionBarToggle must not be null");
        mNavigationView.setNavigationItemSelectedListener(setListener());
        mNavigationView.inflateMenu(provideResMenu());
        mDrawerLayout.addDrawerListener(mActionBarToggle);
        mActionBarToggle.syncState();
    }

    public abstract NavigationView setNavigationView();

    public abstract DrawerLayout setDrawerLayout();

    public abstract NavigationView.OnNavigationItemSelectedListener setListener();

    public abstract ActionBarDrawerToggle setActionBarToggle();


    public abstract int provideResMenu();
}
