package ph.seventhunders.applibrary.listeners;

public interface RVObservable {
    void registerObserver(RVObserver o);
    void notifyListenerAttached();
    void removeObserver();
}