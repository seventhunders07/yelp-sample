package ph.seventhunders.applibrary.listeners;

public interface RVObserver {
    void update(RecyclerViewItemClickListener listener);
}