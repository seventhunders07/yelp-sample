package com.mobi.yelp;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.mobi.yelp.activity.BusinessDetailsActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.FailureHandler;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

public class BusinessDetailActivityTest {

    @Rule
    public ActivityTestRule<BusinessDetailsActivity> activityActivityTestRule = new ActivityTestRule<BusinessDetailsActivity>(BusinessDetailsActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent intent = new Intent(targetContext, BusinessDetailsActivity.class);
            intent.putExtra("title", "Test Name");
            intent.putExtra("id", "xaOsjdPfvVnNAy3aEICibA");
            return intent;
        }
    };

    //verifies if views are displayed
    @Test
    public void verifyView(){
        Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(2000));
        Espresso.onView(ViewMatchers.withId(R.id.txtBusinessName)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.ratingData)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.txtReviewCount)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.txtDisplayAddress)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.txtCity)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.txtCategory)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));

        Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(1000));
        Espresso.onView(ViewMatchers.withId(R.id.rvHoursOpen)).withFailureHandler((error, viewMatcher)
                -> error.printStackTrace()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    //delay function for waiting api details
    private static ViewAction waitFor(long delay) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for " + delay + "milliseconds";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(delay);
            }
        };
    }
}
