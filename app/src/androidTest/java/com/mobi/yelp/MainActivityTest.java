package com.mobi.yelp;


import android.view.View;

import com.mobi.yelp.activity.BusinessDetailsActivity;
import com.mobi.yelp.activity.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.intent.matcher.IntentMatchers;
import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.espresso.matcher.ViewMatchers;

import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;


public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> activityIntentsTestRule =
            new IntentsTestRule<>(MainActivity.class);

    //verify if scrolling and clickable
    //verifies if the intended action is correct
    @Test
    public void scrollListAndItemClickTest() {
        Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(10000));
        Espresso.onView(ViewMatchers.withId(R.id.businessList))
                .check(ViewAssertions.matches(hasDescendant(ViewMatchers.withId(R.id.txtBusinessName))))
                .perform(ViewActions.swipeUp()).withFailureHandler((error, viewMatcher) -> error.printStackTrace());
        Espresso.onView(ViewMatchers.withId(R.id.businessList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.actionWithAssertions(ViewActions.click())));
        Intents.intended(IntentMatchers.hasComponent(BusinessDetailsActivity.class.getName()));

    }

    //delay function for waiting api details
    private static ViewAction waitFor(long delay) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for " + delay + "milliseconds";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(delay);
            }
        };
    }

}
