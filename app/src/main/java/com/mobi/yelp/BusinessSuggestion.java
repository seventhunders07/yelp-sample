package com.mobi.yelp;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

public class BusinessSuggestion implements SearchSuggestion {

    private String mColorName;
    private boolean mIsHistory = false;

    public BusinessSuggestion(String suggestion) {
        this.mColorName = suggestion.toLowerCase();
    }

    public BusinessSuggestion(Parcel source) {
        this.mColorName = source.readString();
        this.mIsHistory = source.readInt() != 0;
    }

    public void setIsHistory(boolean isHistory) {
        this.mIsHistory = isHistory;
    }

    public boolean getIsHistory() {
        return this.mIsHistory;
    }

    @Override
    public String getBody() {
        return mColorName;
    }

    public static final Creator<BusinessSuggestion> CREATOR = new Creator<BusinessSuggestion>() {
        @Override
        public BusinessSuggestion createFromParcel(Parcel in) {
            return new BusinessSuggestion(in);
        }

        @Override
        public BusinessSuggestion[] newArray(int size) {
            return new BusinessSuggestion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mColorName);
        dest.writeInt(mIsHistory ? 1 : 0);
    }
}