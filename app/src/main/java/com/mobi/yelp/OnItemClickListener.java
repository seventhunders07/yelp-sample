package com.mobi.yelp;

public interface OnItemClickListener<T> {

    void onClick(T itemData);
}
