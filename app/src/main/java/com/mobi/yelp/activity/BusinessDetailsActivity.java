package com.mobi.yelp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobi.yelp.R;
import com.mobi.yelp.adapter.RVHoursOpenAdapter;
import com.mobi.yelp.model.Category;
import com.mobi.yelp.viewmodel.BusinessViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import ph.seventhunders.applibrary.custom.BaseActivity;

public class BusinessDetailsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.headerImage)
    AppCompatImageView mHeaderImage;

    @BindView(R.id.txtBusinessName)
    AppCompatTextView mBusinessName;

    @BindView(R.id.txtReviewCount)
    AppCompatTextView mReviewCount;

    @BindView(R.id.ratingData)
    AppCompatRatingBar mRatingData;

    @BindView(R.id.txtCategory)
    AppCompatTextView mCategory;

    @BindView(R.id.txtDisplayAddress)
    AppCompatTextView mDisplayAddress;

    @BindView(R.id.txtCity)
    AppCompatTextView mCity;

    @BindView(R.id.rvHoursOpen)
    RecyclerView mRVHoursOpen;

    @BindView(R.id.txtContactNumber)
    AppCompatTextView mContactNumber;

    BusinessViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public int setLayoutResource() {
        return R.layout.activity_business_details;
    }

    private void init() {
        String title = getIntent().getStringExtra("title");
        String id = getIntent().getStringExtra("id");
        mViewModel = ViewModelProviders.of(this).get(BusinessViewModel.class);
        mViewModel.getBusinessDetails(id).observe(this, establishmentDetailsModel -> {
            Picasso.get().load(!establishmentDetailsModel.getImageUrl().isEmpty() ? establishmentDetailsModel.getImageUrl() : "https://reservation.asiwebres.com/v4/App_Themes/images/noimage.png").fit().noPlaceholder().into(mHeaderImage);
            mBusinessName.setText(establishmentDetailsModel.getName());
            mReviewCount.setText(establishmentDetailsModel.getReviewCount() + " Reviews");
            mRatingData.setRating((float) establishmentDetailsModel.getRating());

            List<Category> categoryList = establishmentDetailsModel.getCategories();
            List<String> addressList = establishmentDetailsModel.getLocation().getDisplayAddress();
            StringBuilder categories = new StringBuilder();
            StringBuilder displayAddress = new StringBuilder();
            for (int i = 0; i < categoryList.size(); i++) {
                if ((i + 1) < categoryList.size()) {
                    categories.append(categoryList.get(i).getTitle()).append(", ");
                    continue;
                }

                categories.append(categoryList.get(i).getTitle());
            }
            for (int i = 0; i < addressList.size(); i++) {
                if ((i + 1) < addressList.size()) {
                    displayAddress.append(addressList.get(i)).append(" ");
                    continue;
                }

                displayAddress.append(addressList.get(i));
            }

            mCategory.setText(categories.toString());
            mDisplayAddress.setText(displayAddress.toString());
            mCity.setText(establishmentDetailsModel.getLocation().getCity());
            mContactNumber.setText(establishmentDetailsModel.getDisplayPhone());

            if (establishmentDetailsModel.getHours() != null) {
                mRVHoursOpen.setAdapter(new RVHoursOpenAdapter(establishmentDetailsModel.getHours().get(0).getOpen()));
                mRVHoursOpen.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            }
        });
        setToolbar(mToolbar);
        setToolbarTitle(title, true);


    }

    public static void startActivity(Context context, String title, String id) {
        Intent i = new Intent(context, BusinessDetailsActivity.class);
        i.putExtra("title", title);
        i.putExtra("id", id);
        context.startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
