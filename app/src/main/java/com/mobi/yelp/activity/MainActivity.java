package com.mobi.yelp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuInflater;
import android.widget.PopupMenu;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.util.Util;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.mobi.yelp.BusinessSuggestion;
import com.mobi.yelp.R;
import com.mobi.yelp.adapter.RVBusinessAdapter;
import com.mobi.yelp.model.Business;
import com.mobi.yelp.viewmodel.BusinessViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import ph.seventhunders.applibrary.custom.BaseActivity;
import ph.seventhunders.applibrary.utils.EqualSpacingItemDecoration;

public class MainActivity extends BaseActivity {

    private static final int REQUEST_CODE_CHECK_SETTINGS = 777;
    private static final long LOCATION_UPDATE_INTERVAL = 200;
    private static final long LOCATION_UPDATE_FASTEST_INTERVAL = 0;

    @BindView(R.id.businessList)
    RecyclerView mBusinessList;

    @BindView(R.id.floating_search_view)
    FloatingSearchView mFloatingSearchView;

    private RVBusinessAdapter mAdapter;

    private BusinessViewModel mViewModel;
    private FusedLocationProviderClient mFusedClient;
    private LocationRequest mLocationRequest;
    private LatLng currentLatLng;
    private String query = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public int setLayoutResource() {
        return R.layout.activity_main;
    }

    private void init() {

        mFusedClient = new FusedLocationProviderClient(this);

        mAdapter = new RVBusinessAdapter(itemData -> {
            BusinessDetailsActivity.startActivity(MainActivity.this, itemData.getName(), itemData.getId());
        });
        mViewModel = ViewModelProviders.of(this).get(BusinessViewModel.class);
        mAdapter.setDataset(new ArrayList<>());
        LinearLayoutManager llm = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, RecyclerView.VERTICAL);
        EqualSpacingItemDecoration equalSpacingItemDecoration = new EqualSpacingItemDecoration(8, 1);
        mBusinessList.setLayoutManager(llm);
        mBusinessList.addItemDecoration(dividerItemDecoration);
        mBusinessList.addItemDecoration(equalSpacingItemDecoration);
        mBusinessList.setAdapter(mAdapter);

        mFloatingSearchView.setOnQueryChangeListener((oldQuery, newQuery) -> {
            query = newQuery;
            if (newQuery.length() > 3 && currentLatLng != null) {
                loadBusinessList(newQuery);
            }
        });

        mFloatingSearchView.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_location:
                    updateLatLng();
                    break;
                case R.id.action_filter:
                    showFilterMenu();
                    break;
            }
        });

        mFloatingSearchView.setDimBackground(false);

        checkForPermissions(111, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @Override
    protected void permissionGranted(int requestCode) {
        if (requestCode == 111) {
            enableLocationSettings();
        }
    }

    @SuppressLint("MissingPermission")
    private void updateLatLng() {
        final long startTime = System.currentTimeMillis();
        LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                // TODO: Those lines of code will run on the background thread.
                if ((locationResult.getLastLocation() != null) && (System.currentTimeMillis() <= startTime + 30 * 1000)) {
                    currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    loadBusinessList("");
                }
                // TODO: After receiving location result, remove the listener.
                mFusedClient.removeLocationUpdates(this);
            }
        };
        mFusedClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());
    }

    private void enableLocationSettings() {
        mLocationRequest = LocationRequest.create()
                .setMaxWaitTime(0)
                .setInterval(LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(LOCATION_UPDATE_FASTEST_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        LocationServices
                .getSettingsClient(this)
                .checkLocationSettings(builder.build())
                .addOnSuccessListener(this, locationSettingsResponse -> updateLatLng())
                .addOnFailureListener(this, ex -> {
                    if (ex instanceof ResolvableApiException) {
                        // Location settings are NOT satisfied,  but this can be fixed  by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) ex;
                            resolvable.startResolutionForResult(MainActivity.this, REQUEST_CODE_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                });
    }

    private void showFilterMenu() {
        PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.toolbar), Gravity.RIGHT);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_filter, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.filter_rating:
                    item.setChecked(true);
                    loadBusinessList(query, "rating");
                    return true;
                case R.id.filter_distance:
                    item.setChecked(true);
                    loadBusinessList(query, "distance");
                    return true;
            }
            return false;
        });
        popupMenu.show();
    }

    private void loadBusinessList(String term, String filterBy) {
        mViewModel.searchBusinesses(term, currentLatLng.latitude, currentLatLng.longitude, filterBy).observe(this, businessesModel -> {
            mAdapter.setDataset(businessesModel.getBusinesses());
            mAdapter.notifyDataSetChanged();
        });
    }

    private void loadBusinessList(String term) {
        mViewModel.searchBusinesses(term, currentLatLng.latitude, currentLatLng.longitude).observe(this, businessesModel -> {
            mAdapter.setDataset(businessesModel.getBusinesses());
            mAdapter.notifyDataSetChanged();
            List<BusinessSuggestion> businessSuggestions = new ArrayList<>();
            for (Business business : businessesModel.getBusinesses()) {
                businessSuggestions.add(new BusinessSuggestion(business.getName()));
            }
        });
    }

}
