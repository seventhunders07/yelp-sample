package com.mobi.yelp.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobi.yelp.OnItemClickListener;
import com.mobi.yelp.R;
import com.mobi.yelp.model.Business;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;

import butterknife.BindView;
import ph.seventhunders.applibrary.custom.RecyclerViewBaseAdapter;
import ph.seventhunders.applibrary.custom.RecyclerViewBaseViewHolder;
import ph.seventhunders.applibrary.listeners.RVObservable;

public class RVBusinessAdapter extends RecyclerViewBaseAdapter<Business, RVBusinessAdapter.RVBusinessViewModel> {

    OnItemClickListener<Business> mItemClickListener;

    public RVBusinessAdapter(OnItemClickListener<Business> itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public RVBusinessViewModel onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_business, parent, false);
        return new RVBusinessViewModel(view, this);
    }

    @Override
    public void onBindRecyclerViewHolder(RVBusinessViewModel holder, int position) {
        Business business = items.get(position);
        StringBuilder sb = new StringBuilder();

        holder.mTxtBusinessName.setText(business.getName());
        holder.mRatingData.setRating(business.getRating().floatValue());
        Picasso.get().load(!business.getImageUrl().isEmpty() ? business.getImageUrl() : "https://reservation.asiwebres.com/v4/App_Themes/images/noimage.png").centerCrop().resize(700, 500).noPlaceholder().into(holder.mImageThumb);
        if (business.getIsClosed()) {
            holder.mIsOpenNow.setText("Closed");
            holder.mIsOpenNow.setTextColor(Color.RED);
        }
        List<String> displayAddress = business.getLocation().getDisplayAddress();
        for (int i = 0; i < displayAddress.size(); i++) {
            if ((i + 1) < displayAddress.size()) {
                sb.append(displayAddress.get(i)).append(", ");
                continue;
            }

            sb.append(displayAddress.get(i));
        }
        holder.mTxtAddress.setText(sb.toString());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class RVBusinessViewModel extends RecyclerViewBaseViewHolder {

        @BindView(R.id.txtAddress)
        AppCompatTextView mTxtAddress;

        @BindView(R.id.txtBusinessName)
        AppCompatTextView mTxtBusinessName;

        @BindView(R.id.isOpenNow)
        AppCompatTextView mIsOpenNow;

        @BindView(R.id.ratingData)
        AppCompatRatingBar mRatingData;

        @BindView(R.id.imageThumb)
        AppCompatImageView mImageThumb;

        protected RVBusinessViewModel(View itemView, RVObservable observable) {
            super(itemView, observable);

            itemView.setOnClickListener(v -> mItemClickListener.onClick(items.get(getAdapterPosition())));
        }
    }
}
