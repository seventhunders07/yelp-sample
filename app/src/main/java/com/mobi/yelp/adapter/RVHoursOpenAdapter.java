package com.mobi.yelp.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobi.yelp.R;
import com.mobi.yelp.model.Open;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RVHoursOpenAdapter extends RecyclerView.Adapter<RVHoursOpenAdapter.RVHoursOpenViewHolder> {

    List<Open> openList;

    public RVHoursOpenAdapter(List<Open> openList) {
        this.openList = openList;
    }

    @NonNull
    @Override
    public RVHoursOpenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hours, parent, false);
        return new RVHoursOpenViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVHoursOpenViewHolder holder, int position) {
        Open openData = openList.get(position);
        holder.mTxtDay.setText(formatDay(openData.getDay()));
        holder.mTxtHoursOpen.setText(formatHours(openData.getStart()) + " - " + formatHours(openData.getEnd()));
        if(isToday(formatDay(openData.getDay()))){
            holder.itemView.setBackgroundColor(Color.parseColor("#5cb85c"));
            holder.mTxtDay.setTextColor(Color.WHITE);
            holder.mTxtHoursOpen.setTextColor(Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        return openList.size();
    }

    private String formatDay(int day) {
        switch (day) {
            case 0:
                return "Monday";
            case 1:
                return "Tuesday";
            case 2:
                return "Wednesday";
            case 3:
                return "Thursday";
            case 4:
                return "Friday";
            case 5:
                return "Saturday";
            case 6:
                return "Sunday";
            default:
                return "";
        }
    }

    private boolean isToday(String day){
        Calendar now = Calendar.getInstance();
        return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(now.getTime()).equals(day);
    }

    private String formatHours(String hours) {
        int parsedHours = Integer.parseInt(hours);
        int hour = parsedHours / 100;
        int minutes = (parsedHours - hour * 100) % 60;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);

        return new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(calendar.getTime());
    }

    class RVHoursOpenViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtDay)
        TextView mTxtDay;

        @BindView(R.id.txtHoursOpen)
        TextView mTxtHoursOpen;

        public RVHoursOpenViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
