package com.mobi.yelp.api;

import com.mobi.yelp.model.BusinessesModel;
import com.mobi.yelp.model.EstablishmentDetailsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Routes {

    @GET("businesses/search")
    Call<BusinessesModel> getBusinesses(@Query("term") String term, @Query("latitude") double lat, @Query("longitude") double lng);

    @GET("businesses/search")
    Call<BusinessesModel> getBusinesses(@Query("term") String term, @Query("latitude") double lat, @Query("longitude") double lng, @Query("sort_by") String sort_by);

    @GET("businesses/{id}")
    Call<EstablishmentDetailsModel> getBusinessDetail(@Path("id") String id);

}
