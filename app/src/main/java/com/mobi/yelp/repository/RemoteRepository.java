package com.mobi.yelp.repository;

import android.content.Context;

import com.mobi.yelp.api.Routes;
import com.mobi.yelp.model.BusinessesModel;
import com.mobi.yelp.model.EstablishmentDetailsModel;
import com.mobi.yelp.viewmodel.BusinessViewModel;

import ph.seventhunders.applibrary.rest.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteRepository {

    private Context mContext;
    private BusinessViewModel mBusinessViewModel;

    public RemoteRepository(Context context, BusinessViewModel businessViewModel) {
        mContext = context;
        mBusinessViewModel = businessViewModel;
    }

    public void loadBusinesses(String term, double lat, double lng){
        RestClient.create(mContext, Routes.class, true).getBusinesses(term, lat, lng, "rating").enqueue(new Callback<BusinessesModel>() {
            @Override
            public void onResponse(Call<BusinessesModel> call, Response<BusinessesModel> response) {
                if(response.isSuccessful()){
                    mBusinessViewModel.setBusinessModel(response.body());
                }
            }

            @Override
            public void onFailure(Call<BusinessesModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void loadBusinesses(String term, double lat, double lng, String filterBy){
        RestClient.create(mContext, Routes.class, true).getBusinesses(term, lat, lng, filterBy).enqueue(new Callback<BusinessesModel>() {
            @Override
            public void onResponse(Call<BusinessesModel> call, Response<BusinessesModel> response) {
                if(response.isSuccessful()){
                    mBusinessViewModel.setBusinessModel(response.body());
                }
            }

            @Override
            public void onFailure(Call<BusinessesModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getBusinessDetail(String id){
        RestClient.create(mContext, Routes.class, true).getBusinessDetail(id).enqueue(new Callback<EstablishmentDetailsModel>() {
            @Override
            public void onResponse(Call<EstablishmentDetailsModel> call, Response<EstablishmentDetailsModel> response) {
                if(response.isSuccessful()){
                    mBusinessViewModel.setBusinessDetail(response.body());
                }
            }

            @Override
            public void onFailure(Call<EstablishmentDetailsModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
