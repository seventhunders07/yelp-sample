package com.mobi.yelp.viewmodel;

import android.app.Application;
import android.content.Context;

import com.mobi.yelp.model.BusinessesModel;
import com.mobi.yelp.model.EstablishmentDetailsModel;
import com.mobi.yelp.repository.RemoteRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class BusinessViewModel extends AndroidViewModel {

    private RemoteRepository mRemoteRepository;
    private MutableLiveData<BusinessesModel> mBusinessModel;
    private MutableLiveData<EstablishmentDetailsModel> mEstablishmentDetails;

    public BusinessViewModel(@NonNull Application application) {
        super(application);
        init(application.getApplicationContext());
    }

    private void init(Context context) {
        mRemoteRepository = new RemoteRepository(context, this);
    }

    public LiveData<BusinessesModel> searchBusinesses(String term, double lat, double lng) {
        if (mBusinessModel == null) {
            mBusinessModel = new MutableLiveData<>();
        }

        mRemoteRepository.loadBusinesses(term, lat, lng);
        return mBusinessModel;
    }

    public LiveData<BusinessesModel> searchBusinesses(String term, double lat, double lng, String filter) {
        if (mBusinessModel == null) {
            mBusinessModel = new MutableLiveData<>();
        }

        mRemoteRepository.loadBusinesses(term, lat, lng, filter);
        return mBusinessModel;
    }

    public LiveData<EstablishmentDetailsModel> getBusinessDetails(String id){
        if(mEstablishmentDetails == null){
            mEstablishmentDetails = new MutableLiveData<>();
        }
        mRemoteRepository.getBusinessDetail(id);
        return mEstablishmentDetails;
    }

    public void setBusinessModel(BusinessesModel businessesModel){
        mBusinessModel.postValue(businessesModel);
    }

    public void setBusinessDetail(EstablishmentDetailsModel establishmentDetailsModel){
        mEstablishmentDetails.postValue(establishmentDetailsModel);
    }


}
